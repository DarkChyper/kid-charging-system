# Kid Charging System

Code arduino d'une borne de recharge fictive pour les voitures d'enfant. 

## Exemple
Ce genre de jouet :

<img src="voiture.jpg" alt="Une voiture d'enfant avec l'enfant dedans" width="200"/>

Un montage avec un arduino nano sur une breadboard :

<img src="example_fonctionnement.gif" alt="Exemple d'un montage avec breadboard" width="400"/>

## Déploiement

Utilisez l'IDE Arduino pour charger le code sur le micro-controleur.

https://www.arduino.cc/en/software


## License

Le code de ce dépôt est sous licence Creative Commons BY 4.0, voir `LICENCE.md`

